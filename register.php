<?php
namespace cpl\testcase_52811;

function cp_register_agenda() {
	$labels = array(
		'name'                   => __( 'Agendas', 'CPDOMAIN' ),
		'singular_name'          => __( 'Agenda', 'CPDOMAIN' ),
		'all_items'              => __( 'All Agendas', 'CPDOMAIN' ),
		'attributes'             => __( 'Attributes', 'CPDOMAIN' ),
		'add_new'                => __( 'Add new agenda', 'CPDOMAIN' ),
		'edit_item'              => __( 'Edit agenda', 'CPDOMAIN' ),
		'view_item'              => __( 'View agenda', 'CPDOMAIN' ),
		'view_items'             => __( 'View agendas', 'CPDOMAIN' ),
		'search_items'           => __( 'Search past agendas', 'CPDOMAIN' ),
		'item_reverted_to_draft' => __( 'Agenda is now a draft', 'CPDOMAIN' ),
		'item_updated'           => __( 'Agenda updates saved', 'CPDOMAIN' ),
		'item_published'         => __( 'Agenda is published to the public', 'CPDOMAIN' ),
		'archives'               => __( 'Past Agendas', 'CPDOMAIN' ),
		'attributes'             => __( 'Agenda Attributes', 'CPDOMAIN' ),
	);

	$args = array(
		'labels'              => $labels,
		'public'              => true,
		'has_archive'         => true,
		'supports'            => array( 'editor', 'title', 'custom-fields', 'revisions' ),
		'delete_with_user'    => false,
		'exclude_from_search' => true,
		'public'              => true,
		'show_in_rest'        => true,
		'rewrite'             => array( 'slug' => 'agendas' ),
		'template'            => array(
			array(
				'core/paragraph',
				array(
					'align'   => 'center',
					'content' => 'Please be aware this is a test.',
				),
			),
			array(
				'core/paragraph',
				array(
					'align'   => 'center',
					'content' => 'A second paragraph that has content inserted',
				),
			),
		),
	);
		register_post_type( 'cpl_agenda', $args );
}

